## Summary

This project is a Java microservice with Spring Boot in which the dependency management is done through Maven. The
project consists of three layers; controller, service and persistence layer. The persistence layer
is done using JPA and **spring data jpa** library. The database is an **H2 memory database** for which there is an
autoloading file *data.sql* in the path ```resources/data.sql```.

Once the microservice starts, you can access H2 console using the
following paths:

```
http://localhost:8082/h2-console
```

The request paths are following

**Health check endpoint**
```
GET http://localhost:8082/actuator/health
```

**To get a product by id**
```
GET http://localhost:8082/api/v1/products/{{id}}
```

**To create a new Product**

```
POST http://localhost:8082/api/v1/products
```
with json body example
```
{
       
        "name": "SAMSUNG GALAXY",
        "description": "lorem ipsum",
        "price": 10.99,
        "categoryId": 2
}
```

**For Update a Product**

```
PUT http://localhost:8082/api/v1/products/{{id}}
```
with json body example
```
{
       
        "name": "SAMSUNG GALAXY UPDATED",
        "description": "lorem ipsum updated",
        "price": 20.99,
        "categoryId": 1
}
```


### This Project manage two profiles dev and prod.

The database credentials are in the file ```application-{profile}}.yml```