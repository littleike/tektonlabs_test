package com.tektonlabs.tektonlabs_test.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import static com.tektonlabs.tektonlabs_test.utils.StringsUtils.toIndentedString;

@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class CategoryResponse {

    @JsonProperty("categoryId")
    private Long categoryId;

    @JsonProperty("name")
    private String name;



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryResponse categoryResponse = (CategoryResponse) o;
        return Objects.equals(this.categoryId, categoryResponse.categoryId) &&
                Objects.equals(this.name, categoryResponse.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryId, name);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CategoryResponse {\n");

        sb.append("    categoryId: ").append(toIndentedString(categoryId)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("}");
        return sb.toString();
    }


}
