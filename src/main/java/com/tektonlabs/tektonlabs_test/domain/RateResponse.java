package com.tektonlabs.tektonlabs_test.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import static com.tektonlabs.tektonlabs_test.utils.StringsUtils.toIndentedString;

@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class RateResponse {

    @JsonProperty("rateId")
    private Long rateId;

    @JsonProperty("rate")
    private Double rate;

    @JsonProperty("count")
    private Double count;



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RateResponse rateResponse = (RateResponse) o;
        return Objects.equals(this.rateId, rateResponse.rateId) &&
                Objects.equals(this.rate, rateResponse.rate) &&
                Objects.equals(this.count, rateResponse.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rateId, rate,  count);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RateResponse {\n");

        sb.append("    rateId: ").append(toIndentedString(rateId)).append("\n");
        sb.append("    rate: ").append(toIndentedString(rate)).append("\n");
        sb.append("    count: ").append(toIndentedString(count)).append("\n");
        sb.append("}");
        return sb.toString();
    }


}
