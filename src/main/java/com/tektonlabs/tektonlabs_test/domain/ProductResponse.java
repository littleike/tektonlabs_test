package com.tektonlabs.tektonlabs_test.domain;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.tektonlabs.tektonlabs_test.utils.StringsUtils.toIndentedString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse   {
    @JsonProperty("productId")
    private Long productId;

    @JsonProperty("description")
    private String description;

    @JsonProperty("name")
    private String name;

    @JsonProperty("price")
    private Double price;

    @JsonProperty("category")
    private CategoryResponse category;

    @JsonProperty("rating")
    private RateResponse rate;

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("productUrl")
    private String productUrl;

    @JsonProperty("stock")
    private Double stock;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductResponse productResponse = (ProductResponse) o;
        return Objects.equals(this.productId, productResponse.productId) &&
                Objects.equals(this.name, productResponse.name) &&
                Objects.equals(this.description, productResponse.description) &&
                Objects.equals(this.price, productResponse.price) &&
                Objects.equals(this.category, productResponse.category) &&
                Objects.equals(this.rate, productResponse.rate) &&
                Objects.equals(this.productCode, productResponse.productCode) &&
                Objects.equals(this.productUrl, productResponse.productUrl) &&
                Objects.equals(this.stock, productResponse.stock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, name, description, price, rate, category, productCode, productUrl, stock);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProductResponse {\n");

        sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    price: ").append(toIndentedString(price)).append("\n");
        sb.append("    category: ").append(toIndentedString(category)).append("\n");
        sb.append("    rate: ").append(toIndentedString(rate)).append("\n");
        sb.append("    productCode: ").append(toIndentedString(productCode)).append("\n");
        sb.append("    productUrl: ").append(toIndentedString(productUrl)).append("\n");
        sb.append("    stock: ").append(toIndentedString(stock)).append("\n");
        sb.append("}");
        return sb.toString();
    }


}

