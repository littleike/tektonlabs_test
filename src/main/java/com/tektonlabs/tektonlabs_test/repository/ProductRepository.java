package com.tektonlabs.tektonlabs_test.repository;

import com.tektonlabs.tektonlabs_test.repository.entities.Product;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository()
@Qualifier("products")
public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findProductByProductId(final Long id);
}
