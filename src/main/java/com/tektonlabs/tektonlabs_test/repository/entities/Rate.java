package com.tektonlabs.tektonlabs_test.repository.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Entity(name = "Rate")
public class Rate {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "rate")
    private Double rate;

    @Column(name = "count")
    private Double count;


    @OneToOne( fetch = FetchType.LAZY, mappedBy = "rating")
    private Product product;

}