package com.tektonlabs.tektonlabs_test.repository.entities;


import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Entity(name = "CATEGORY")
@Builder
public class Category {

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="NAME")
    private String name;


    @OneToMany( fetch = FetchType.LAZY, mappedBy = "category")
    private Set<Product> products;
}
