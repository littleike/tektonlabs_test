package com.tektonlabs.tektonlabs_test.repository.entities;


import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Entity(name = "PRODUCTS")
@Builder
public class Product {

    @Id
    @Column(name = "PRODUCT_ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long productId;

    @Column(name = "NAME")
    @NotNull
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRICE")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", nullable = false)
    private Category category;

    @OneToOne
    @JoinColumn(name = "RATE_ID", nullable = true)
    private Rate rating;

}
