package com.tektonlabs.tektonlabs_test.repository.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class ApiProduct {

    @JsonProperty("product_code")
    private String productCode;

    @JsonProperty("product_url")
    private String productUrl;

    @JsonProperty("stock")
    private Double stock;

    public String toJsonString(){
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"product_code\":\""+productCode+"\",");
        sb.append("\"product_url\":\""+productUrl+"\",");
        sb.append("\"stock\":\""+stock+"\",");
        sb.append("}");
        return sb.toString();
    }
}
