package com.tektonlabs.tektonlabs_test.repository;


import com.tektonlabs.tektonlabs_test.repository.entities.Category;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository()
@Qualifier("categories")
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
