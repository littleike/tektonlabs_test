package com.tektonlabs.tektonlabs_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TektonlabsTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TektonlabsTestApplication.class, args);
	}

}
