package com.tektonlabs.tektonlabs_test.service;

import com.tektonlabs.tektonlabs_test.domain.ProductResponse;
import com.tektonlabs.tektonlabs_test.web.form.ProductForm;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("productService")
public interface ProductService {

    ProductResponse getProductById(final Long id);
    ProductResponse createProduct(final ProductForm product);
    ProductResponse updateProduct(final Long id,final ProductForm productForm);
}
