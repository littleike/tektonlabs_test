package com.tektonlabs.tektonlabs_test.service.imp;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.tektonlabs.tektonlabs_test.domain.CategoryResponse;
import com.tektonlabs.tektonlabs_test.domain.ProductResponse;
import com.tektonlabs.tektonlabs_test.repository.CategoryRepository;
import com.tektonlabs.tektonlabs_test.repository.ProductRepository;
import com.tektonlabs.tektonlabs_test.repository.api.ApiProduct;
import com.tektonlabs.tektonlabs_test.repository.entities.Category;
import com.tektonlabs.tektonlabs_test.repository.entities.Product;
import com.tektonlabs.tektonlabs_test.repository.entities.Rate;
import com.tektonlabs.tektonlabs_test.service.EntityMapperService;
import com.tektonlabs.tektonlabs_test.service.ProductService;
import com.tektonlabs.tektonlabs_test.service.RetoolApiService;
import com.tektonlabs.tektonlabs_test.web.exceptions.CategoryNotFoundException;
import com.tektonlabs.tektonlabs_test.web.form.ProductForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    RetoolApiService retoolApiService;

    @Autowired
    EntityMapperService entityMapperService;

    private static final Cache<Long, ProductResponse> PRODUCT_CACHE_BY_ID = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.MINUTES).build();

    private static final ProductResponse FALLBACK = ProductResponse.builder().productId(0L).build();

    @Override
    public ProductResponse getProductById(final Long id) {

        try {
            final ProductResponse result = PRODUCT_CACHE_BY_ID.get(id, fromDatabase(id));
            return (result.getProductId() == 0) ? null : result;
        } catch (ExecutionException e) {
            log.error("Error getting Products from Database: " + id, e);
        }
        return null;
    }

    @Override
    public ProductResponse createProduct(final ProductForm product) {

        Product newProduct = productRepository.save(fromProductForm(product));
        /* TODO logic to create product on external service and manage creation between both databases*/

       return getMappedProduct(newProduct, null);
    }

    @Override
    public ProductResponse updateProduct(final Long id,final ProductForm productForm) {
        Product productToUpdate = fromProductForm(productForm);
        productToUpdate.setProductId(id);
        Product updatedProduct = productRepository.save(productToUpdate);
        /* TODO logic to create product on external service and manage Update between both databases*/

        return getMappedProduct(updatedProduct, null);
    }

    private Callable<ProductResponse> fromDatabase(final Long id){
        return () -> {
            Optional<Product> product =  productRepository.findProductByProductId(id);
            return product.map(value ->
                            getMappedProduct(value, retoolApiService.getProductById(product.get().getProductId())))
                    .orElse(FALLBACK);
        };
    }

    private ProductResponse getMappedProduct(@NotNull final Product product, final ApiProduct apiProduct){

        ProductResponse result = entityMapperService.productToResponse(product);
        if(!Objects.isNull(apiProduct)){
            result.setProductCode(apiProduct.getProductCode());
            result.setProductUrl(apiProduct.getProductUrl());
            result.setStock(apiProduct.getStock());
        }
        return result;
    }

    private Product fromProductForm(final ProductForm productForm) {

        Category category = categoryRepository.findById(productForm.getCategoryId()).orElseThrow(CategoryNotFoundException::new);

        return Product.builder()
                .description(productForm.getDescription())
                .name(productForm.getName())
                .price(productForm.getPrice())
                .category(category)
                .build();
    }
}
