package com.tektonlabs.tektonlabs_test.service.imp;

import com.tektonlabs.tektonlabs_test.repository.api.ApiProduct;
import com.tektonlabs.tektonlabs_test.service.RetoolApiService;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
@Slf4j
public class RetoolApiServiceImpl implements RetoolApiService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public ApiProduct getProductById(@NotNull @Valid final Long id) {

        return Try.of(() -> restTemplate.getForObject("/" + id, ApiProduct.class))
                .onFailure( e -> log.error("Error on RetoolApiServiceImpl consuming Retool API: " + e.getMessage()))
                .getOrNull();

    }

    @Override
    public ApiProduct createNewProduct(ApiProduct product) {
        MultiValueMap<String, String> headers = null;
        HttpEntity<String> request = new HttpEntity<String>(product.toJsonString(), headers);
        ResponseEntity<ApiProduct> apiProductResponseEntity = Try.of(() -> restTemplate.postForEntity("/", request, ApiProduct.class))
                .onFailure( e -> log.error("Error on RetoolApiServiceImpl consuming Retool API: " + e.getMessage()))
                .getOrNull();
        return apiProductResponseEntity.getBody();
    }


}
