package com.tektonlabs.tektonlabs_test.service.imp;

import com.tektonlabs.tektonlabs_test.domain.CategoryResponse;
import com.tektonlabs.tektonlabs_test.domain.ProductResponse;
import com.tektonlabs.tektonlabs_test.domain.RateResponse;
import com.tektonlabs.tektonlabs_test.repository.entities.Category;
import com.tektonlabs.tektonlabs_test.repository.entities.Product;
import com.tektonlabs.tektonlabs_test.repository.entities.Rate;
import com.tektonlabs.tektonlabs_test.service.EntityMapperService;
import org.springframework.stereotype.Service;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Service
public class EntityMapperServiceImpl implements EntityMapperService {

    @Override
    public CategoryResponse categoryToResponse(@NotNull Category category) {
        return  CategoryResponse.builder()
                .categoryId(category.getId())
                .name(category.getName())
                .build();
    }

    @Override
    public RateResponse rateToResponse(@NotNull Rate rate) {
        return RateResponse.builder()
                .rateId(rate.getId())
                .rate(rate.getRate())
                .count(rate.getCount())
                .build();
    }

    @Override
    public ProductResponse productToResponse(@NotNull Product product) {
        return ProductResponse.builder()
                .productId(product.getProductId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .category(categoryToResponse(product.getCategory()))
                .rate(Objects.isNull(product.getRating()) ? null : rateToResponse(product.getRating()))
                .build();
    }
}
