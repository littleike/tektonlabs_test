package com.tektonlabs.tektonlabs_test.service;

import com.tektonlabs.tektonlabs_test.repository.api.ApiProduct;

public interface RetoolApiService {

    ApiProduct getProductById(final Long id);
    ApiProduct createNewProduct(final ApiProduct product);
}
