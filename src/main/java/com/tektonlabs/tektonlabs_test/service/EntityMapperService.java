package com.tektonlabs.tektonlabs_test.service;

import com.tektonlabs.tektonlabs_test.domain.CategoryResponse;
import com.tektonlabs.tektonlabs_test.domain.ProductResponse;
import com.tektonlabs.tektonlabs_test.domain.RateResponse;
import com.tektonlabs.tektonlabs_test.repository.entities.Category;
import com.tektonlabs.tektonlabs_test.repository.entities.Product;
import com.tektonlabs.tektonlabs_test.repository.entities.Rate;

public interface EntityMapperService {

    CategoryResponse categoryToResponse(final Category category);
    RateResponse rateToResponse(final Rate rate);
    ProductResponse productToResponse(final Product product);
}
