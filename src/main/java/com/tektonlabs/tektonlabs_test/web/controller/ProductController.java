package com.tektonlabs.tektonlabs_test.web.controller;

import com.sun.istack.NotNull;
import com.tektonlabs.tektonlabs_test.domain.ProductResponse;
import com.tektonlabs.tektonlabs_test.service.ProductService;
import com.tektonlabs.tektonlabs_test.web.exceptions.ProductNotFoundException;
import com.tektonlabs.tektonlabs_test.web.form.ProductForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    ProductService productService;


    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductResponse> getProductById(@PathVariable @NotNull @Valid Long id) {

        ProductResponse product = productService.getProductById(id);

        return product == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(product);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductResponse> createProduct(@Valid @RequestBody ProductForm productForm) {

        final ProductResponse created = productService.createProduct(productForm);

        return created == null ? ResponseEntity.badRequest().build() : ResponseEntity.status(HttpStatus.CREATED).body(created);
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductResponse> updateProduct(@PathVariable @NotNull @Valid Long id, @Valid @RequestBody ProductForm productForm) {


        final ProductResponse productToUpdate = productService.getProductById(id);
        if(Objects.isNull(productToUpdate)) throw new ProductNotFoundException();

        final ProductResponse updatedProduct = productService.updateProduct(id, productForm);
        return updatedProduct == null ? ResponseEntity.badRequest().build() : ResponseEntity.status(HttpStatus.OK).body(updatedProduct);
    }


}
