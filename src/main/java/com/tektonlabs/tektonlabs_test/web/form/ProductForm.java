package com.tektonlabs.tektonlabs_test.web.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Data
public class ProductForm {

    @JsonProperty("description")
    private String description;

    @JsonProperty("name")
    @NotNull
    private String name;

    @JsonProperty("price")
    private Double price;

    @JsonProperty("categoryId")
    @NotNull
    private Long categoryId;

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("productUrl")
    private String productUrl;

    @JsonProperty("stock")
    private Double stock;

}
