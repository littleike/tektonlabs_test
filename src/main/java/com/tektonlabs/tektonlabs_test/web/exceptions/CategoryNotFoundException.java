package com.tektonlabs.tektonlabs_test.web.exceptions;

import javax.persistence.PersistenceException;

public class CategoryNotFoundException extends PersistenceException {

    public CategoryNotFoundException() {
        super();
    }

    public CategoryNotFoundException( String message ) {
        super(message);
    }


}
