package com.tektonlabs.tektonlabs_test.web.exceptions;

import com.tektonlabs.tektonlabs_test.web.exceptions.CategoryNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {


    @ExceptionHandler( {CategoryNotFoundException.class})
    public ResponseEntity<Object> handleCategoryNotFoundException(
            CategoryNotFoundException ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return new ResponseEntity<Object>(
                "{\"errors\": \"Invalid Id for Category.\"}", headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler( {ProductNotFoundException.class})
    public ResponseEntity<Object> handleProductNotFoundException(
            ProductNotFoundException ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return new ResponseEntity<Object>(
                "{\"errors\": \"Invalid Id for Product.\"}", headers, HttpStatus.NOT_FOUND);
    }


}