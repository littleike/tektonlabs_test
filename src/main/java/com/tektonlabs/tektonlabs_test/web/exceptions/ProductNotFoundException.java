package com.tektonlabs.tektonlabs_test.web.exceptions;


import javax.persistence.PersistenceException;

public class ProductNotFoundException extends PersistenceException {

    public ProductNotFoundException() {
        super();
    }

    public ProductNotFoundException( String message ) {
        super(message);
    }


}