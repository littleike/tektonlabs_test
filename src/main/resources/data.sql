

INSERT INTO CATEGORY (ID,NAME) VALUES(1,'Technology');
INSERT INTO CATEGORY (ID,NAME) VALUES(2,'Garden');
INSERT INTO CATEGORY (ID,NAME) VALUES(3,'Home');

INSERT INTO RATE (ID,RATE,COUNT) VALUES(1,3.9,120);
INSERT INTO RATE (ID,RATE,COUNT) VALUES(2,4.5,200);
INSERT INTO RATE (ID,RATE,COUNT) VALUES(3,3.8,100);

INSERT INTO PRODUCTS (PRODUCT_ID,NAME,DESCRIPTION, PRICE, CATEGORY_ID, RATE_ID) VALUES(1,'IPHONE X','LOREN IPSUM',399.50,1, 1);
INSERT INTO PRODUCTS (PRODUCT_ID,NAME,DESCRIPTION, PRICE, CATEGORY_ID, RATE_ID) VALUES(2,'IPHONE 11','LOREN IPSUM',499.50,1, NULL);
INSERT INTO PRODUCTS (PRODUCT_ID,NAME,DESCRIPTION, PRICE, CATEGORY_ID, RATE_ID) VALUES(3,'IPHONE 12','LOREN IPSUM',799.50,1, 3);
INSERT INTO PRODUCTS (PRODUCT_ID,NAME,DESCRIPTION, PRICE, CATEGORY_ID, RATE_ID) VALUES(4,'IPHONE 13','LOREN IPSUM',999.50,1, 2);
INSERT INTO PRODUCTS (PRODUCT_ID,NAME,DESCRIPTION, PRICE, CATEGORY_ID, RATE_ID) VALUES(5,'SAMSUNG TV 70"','LOREN IPSUM',599.50,1, NULL);
