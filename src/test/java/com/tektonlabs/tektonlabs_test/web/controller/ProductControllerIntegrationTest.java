package com.tektonlabs.tektonlabs_test.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.tektonlabs.tektonlabs_test.ErrorsMap;
import com.tektonlabs.tektonlabs_test.domain.CategoryResponse;
import com.tektonlabs.tektonlabs_test.domain.ProductResponse;
import com.tektonlabs.tektonlabs_test.repository.CategoryRepository;
import com.tektonlabs.tektonlabs_test.repository.ProductRepository;
import com.tektonlabs.tektonlabs_test.repository.entities.Category;
import com.tektonlabs.tektonlabs_test.repository.entities.Product;
import com.tektonlabs.tektonlabs_test.service.EntityMapperService;
import com.tektonlabs.tektonlabs_test.service.ProductService;
import com.tektonlabs.tektonlabs_test.web.exceptions.CategoryNotFoundException;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerIntegrationTest {

    ObjectMapper om = JsonMapper.builder().build();


    @Autowired
    ProductService productService;

    @Autowired
    ProductRepository productRepository;
    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    EntityMapperService entityMapperService;

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getProductByIdRequestThenEvaluateJsonResultObjectTest() throws Exception {

        Optional<Product> expectedRecord = productRepository.findProductByProductId(1L);
        assertTrue(expectedRecord.isPresent());
        assertThat(expectedRecord.get().getProductId()).isEqualTo(1L);
        ProductResponse expectedResult = entityMapperService.productToResponse(expectedRecord.get());

        ProductResponse actualRecord = om.readValue(mockMvc.perform(get("/api/v1/products/1"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(), ProductResponse.class);

        assertThat(actualRecord.getProductId()).isEqualTo(expectedResult.getProductId());
        assertThat(actualRecord.getName()).isEqualTo(expectedResult.getName());
        assertThat(actualRecord.getDescription()).isEqualTo(expectedResult.getDescription());
        assertThat(actualRecord.getPrice()).isEqualTo(expectedResult.getPrice());
        assertThat(actualRecord.getCategory()).isEqualTo(expectedResult.getCategory());


        Optional<Product> expectedRecord2 = productRepository.findProductByProductId(2L);
        assertTrue(expectedRecord2.isPresent());
        assertThat(expectedRecord2.get().getProductId()).isEqualTo(2L);
        ProductResponse expectedResult2 = entityMapperService.productToResponse(expectedRecord2.get());

        ProductResponse actualRecord2 = om.readValue(mockMvc.perform(get("/api/v1/products/2"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(), ProductResponse.class);

        assertThat(actualRecord2.getProductId()).isEqualTo(expectedResult2.getProductId());
        assertThat(actualRecord2.getName()).isEqualTo(expectedResult2.getName());
        assertThat(actualRecord2.getDescription()).isEqualTo(expectedResult2.getDescription());
        assertThat(actualRecord2.getPrice()).isEqualTo(expectedResult2.getPrice());
        assertThat(actualRecord2.getCategory()).isEqualTo(expectedResult2.getCategory());

    }

    @Test
    void createProductThenEvaluateNewProduct() throws Exception {
        Optional<Category> category = categoryRepository.findById(1L);
        assertThat(category).isPresent();
        ProductResponse expectedRecord = ProductResponse.builder()
                .name("SAMSUNG GALAXY")
                .description("lorem ipsum")
                .price(10.99)
                .category(entityMapperService.categoryToResponse(category.get()))
                .build();

        String payload = "{\"name\": \"SAMSUNG GALAXY\",\"description\": \"lorem ipsum\",\"price\": 10.99,\"categoryId\": 1}";
        ProductResponse createdProduct = om.readValue(mockMvc.perform(post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .content(payload))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), ProductResponse.class);

        assertThat(createdProduct.getProductId()).isPositive();
        assertThat(createdProduct.getName()).isEqualTo(expectedRecord.getName());
        assertThat(createdProduct.getDescription()).isEqualTo(expectedRecord.getDescription());
        assertThat(createdProduct.getPrice()).isEqualTo(expectedRecord.getPrice());
        assertThat(createdProduct.getCategory()).isEqualTo(expectedRecord.getCategory());
    }

    @Test
    void sendBadIdForCategory_ThenThrowCategoryNotFoundExceptionTest() throws Exception {
        String payload = "{\"name\": \"SAMSUNG GALAXY\",\"description\": \"lorem ipsum\",\"price\": 10.99,\"categoryId\": 1000}";

        ErrorsMap result = om.readValue(mockMvc.perform(post("/api/v1/products")
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(payload))
                    .andExpect(status().isBadRequest())
                    .andReturn()
                    .getResponse()
                    .getContentAsString(), ErrorsMap.class);


            assertThat(result.getErrors()).isEqualTo("Invalid Id for Category.");

    }

    @Test
    void sendProductToUpdate_ThenAssertionsSucceeds() throws Exception {

        String payload = "{\"name\": \"SAMSUNG GALAXY UPDATED\",\"description\": \"lorem ipsum updated\",\"price\": 20.99,\"categoryId\": 3}";

        Optional<Category> category = categoryRepository.findById(3L);
        assertThat(category).isPresent();
        ProductResponse expectedRecord = ProductResponse.builder()
                .productId(5L)
                .name("SAMSUNG GALAXY UPDATED")
                .description("lorem ipsum updated")
                .price(20.99)
                .category(entityMapperService.categoryToResponse(category.get()))
                .build();

        ProductResponse updatedProduct = om.readValue(mockMvc.perform(put("/api/v1/products/5")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(payload))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(), ProductResponse.class);

        assertThat(updatedProduct.getProductId()).isEqualTo(expectedRecord.getProductId());
        assertThat(updatedProduct.getName()).isEqualTo(expectedRecord.getName());
        assertThat(updatedProduct.getDescription()).isEqualTo(expectedRecord.getDescription());
        assertThat(updatedProduct.getPrice()).isEqualTo(expectedRecord.getPrice());
        assertThat(updatedProduct.getCategory()).isEqualTo(expectedRecord.getCategory());

    }
}