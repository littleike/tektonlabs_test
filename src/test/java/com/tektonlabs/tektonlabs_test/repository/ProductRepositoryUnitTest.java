package com.tektonlabs.tektonlabs_test.repository;

import com.tektonlabs.tektonlabs_test.repository.entities.Category;
import com.tektonlabs.tektonlabs_test.repository.entities.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryUnitTest {


    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void createProductThenEvaluateResultTest() {
        Category category = categoryRepository.getById(1L);
        assertThat(category.getId()).isEqualTo(1L);
        Product saved = this.productRepository.save( Product.builder()
                .name("test")
                .description("test description")
                .price(2.99)
                .category(category)
                .build());
        Product p = this.productRepository.getById(saved.getProductId());
        assertThat(p.getName()).isEqualTo("test");
        assertThat(p.getDescription()).isEqualTo("test description");
        assertThat(p.getPrice()).isEqualTo(2.99);
        assertThat(p.getProductId()).isNotNull();
        assertThat(p.getProductId()).isPositive();
    }
}
