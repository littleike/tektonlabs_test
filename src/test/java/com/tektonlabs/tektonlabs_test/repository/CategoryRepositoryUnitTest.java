package com.tektonlabs.tektonlabs_test.repository;

import com.tektonlabs.tektonlabs_test.repository.entities.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryRepositoryUnitTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void createCategoryThenEvaluateResultTest() {
        Category saved = this.categoryRepository.save(Category.builder()
                .name("test category")
                .build());
        Category c = this.categoryRepository.getById(saved.getId());
        assertThat(c.getName()).isEqualTo("test category");
        assertThat(c.getId()).isNotNull();
        assertThat(c.getId()).isPositive();
    }
}
